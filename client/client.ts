import * as net from 'net';
import clientInputvalidation from './clientInputData';

class ClientConnection {
  socket: any;
  address: any;
  port: any;
  key: any;
  value: any;
  host: any;
  inputData: string[] = [];
  client! :  net.Socket;


  async clientServerConnection() {
    var net = require('net');
    let client = new net.Socket();
    client.connect(this.port, this.address)
    client.on('connect', () => {
      let message: string[] = [];
      message.push("Connection established with server")
      client.write(JSON.stringify(message))
    })
    client.on('data', function (data: any) {
      clientManager.inputOperation(client);
    });
  }

  clientInputData() {
    let inputDataFromClient = clientInputvalidation.getInputForConnectToServer();
    let isDataValid = this.validateInputData(inputDataFromClient);
    if (!isDataValid) {
      clientInputvalidation.inputFormat();
      this.clientInputData();
    }
  }

  inputOperation(client: any) {
    let clientInput =  clientInputvalidation.getInputForGetOrSet();
    let operationType = this.validateTheInputOperation(clientInput);
    if (operationType == "GET") this.sendGetRequestToServer(client);
    else if (operationType == "SET") this.sendSetRequestToServer(client);
    else {
      console.log("Invalid input. Try again!!");
      this.inputOperation(client);
    }
    this.reTry(client);
  }
  reTry(client: any) {
    let clientInput = clientInputvalidation.reTryorClose()
    if (clientInput.split(" ", 1)[0] === "y") {
      this.inputOperation(client);
    }
    else
    console.log('Client closed');
    return this.client.end()
  }
  validateTheInputOperation(clientInput: any) {
    if (
      clientInput.split(" ", 1)[0] == "ics" &&
      clientInput.split(" ", 2)[1] == "GET"
    ) {
      this.key = clientInput.split(" ", 3)[2];
      if (this.key) return "GET";
    }

    if (
      clientInput.split(" ", 1)[0] == "ics" &&
      clientInput.split(" ", 2)[1] == "SET"
    ) {
      this.key = clientInput.split(" ", 3)[2];
      this.value = clientInput.split(" ", 4)[3];
      return "SET";
    }
  }
  sendGetRequestToServer(client: any) {
    let getRequest = {
      type: "GET",
      key: this.key,
    };

    client.write(JSON.stringify(getRequest));
  }

  sendSetRequestToServer(client: any) {
    let putRequest = {
      type: "PUT",
      key: this.key,
      value: this.value,
    };

    client.write(JSON.stringify(putRequest));
  }

  validateInputData(inputData: string) {
    if (
      inputData.split(" ", 1)[0] == "ics" &&
      inputData.split(" ", 2)[1] == "connect"
    ) {
      if (inputData.split(" ", 3)[2] == "-h")
        this.address = inputData.split(" ", 4)[3];

      if (inputData.split(" ", 5)[4] == "-p")
        this.port = inputData.split(" ", 6)[5];

      this.port = parseInt(this.port);
      return true;
    } else {
      return false;
    }
  }
}
let clientManager = new ClientConnection();
clientManager.clientInputData();
clientManager.clientServerConnection();
export default clientManager;