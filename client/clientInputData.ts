import readline from 'readline-sync';
class DataValidation {
     address: any;
     port: any;
     key: any;
     value: any;

     getInputForConnectToServer() {
      return readline.question(`\x1b[33mEnter input Command For Server Connection:\n`);
    }
  
    getInputForGetOrSet() {
      return readline.question(`\x1b[32m\nPlease Enter Command For GET Or SET Data:\n`);
    }

    inputFormat() {
      return readline.question("Input format should be ics connect -h hostname -p portnumber")
    }
  
    reTryorClose() {
      return readline.question(`\x1b[33m\nDo you Want to continue:(y/n)?\n`);
    }

    public  validateInputData(inputData: string) {
        if (
            inputData.split(" ", 1)[0] == "ics" &&
            inputData.split(" ", 2)[1] == "connect"
          ) {
            if (inputData.split(" ", 3)[2] == "-h")
              this.address = inputData.split(" ", 4)[3];
      
            if (inputData.split(" ", 5)[4] == "-p")
              this.port = inputData.split(" ", 6)[5];
      
            this.port = parseInt(this.port);
            return true;
          } else {
            return false;
          }
    }

    public  validateTheInputOperation(clientInput: any) {
        if (
            clientInput.split(" ", 1)[0] == "ics" &&
            clientInput.split(" ", 2)[1] == "GET"
        ) {
            this.key = clientInput.split(" ", 3)[2];
            if (this.address.key) return "GET";
        }

        if (
            clientInput.split(" ", 1)[0] == "ics" &&
            clientInput.split(" ", 2)[1] == "SET"
        ) {
            this.key = clientInput.split(" ", 3)[2];
            this.value = clientInput.split(" ", 4)[3];
            return "SET";
        }
    }

    sendGetRequestToServer(client: any) {
      let getRequest = {
        type: "GET",
        key: this.key,
      };
  
      client.write(JSON.stringify(getRequest));
    }

    sendSetRequestToServer(client: any) {
      let putRequest = {
        type: "PUT",
        key: this.key,
        value: this.value,
      };
      client.write(JSON.stringify(putRequest));
    }
}

let clientInputvalidation : DataValidation = new DataValidation();
export  default clientInputvalidation;