import * as readline from "readline-sync";
import * as net from 'net';
class clientRequestHandler {

     key: any;
     value: any;

    async sendRequestToServer(client: net.Socket, clientInput: string) {
      await client.write(JSON.stringify(clientInput));
    }

    serverConnectionRequests() {
        return readline.question("Enter Input command for server connection:\n");
    }

    inputFormat() {
        return readline.question("Invalid input command!!. Input Format should be **ics connect -h hostname -p portnumber**\n");
    }

    getorSetRequests() {
        return readline.question("Enter input for GET or SET data:\n");
    }

    getRequestFromServer (client: any) {
        let getRequest = {
            type: "GET",
            key: this.key,
          };
        
          client.write(JSON.stringify(getRequest));
        }

    SetRequestToServer(client: any) {
            let putRequest = {
              type: "PUT",
              key: this.key,
              value: this.value,
            };
          
            client.write(JSON.stringify(putRequest));
          }
}
const ClientRequestHandler: clientRequestHandler = new clientRequestHandler();
export default ClientRequestHandler;
