import * as net from 'net';

class ErrorHandler {

    ConnectionError(error: Error, client: net.Socket) {
        console.log("\x1b[31mServer disconnected");
        client.end();
      }

    TimeoutError(client: net.Socket) {
        console.log("\x1b[31m\nPlease restart the server as the connection timeout has expired");
        client.end();
      }    
}

let clientException: ErrorHandler = new ErrorHandler();
export default clientException;