import clientManager from "./client";
import clientException from "./errorHandler"

class ResponseHandler {

  async serverResponse(serverResponse: string) {
    if (serverResponse == 'Invalid Request') {
      console.log("Invalid Request, please re-enter");
    }
    else if (serverResponse == 'Key Not Found') {
      console.log("Key is required.")
    }
    else if (serverResponse == 'Value Not Found') {
      console.log("Value is required.")
    }
    else if (serverResponse == 'Data is not available!') {
      console.log(`\u001b[1;31m\nData is not available!`);
    }
    else if (serverResponse == 'Data stored successfully') {
      console.log(`\u001b[1;33m\nSuccessfully stored data.`)
    }
    else if (serverResponse) {
      console.log(`Value of entered key is = ${serverResponse}`);
    }
    clientManager.reTry("");
  }
}

const responseHandler: ResponseHandler = new ResponseHandler();
export default responseHandler;