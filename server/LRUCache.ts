import mongoDBManager from "./memoryManagement/dataBase/dbOperation";''
import { LRUCache } from "typescript-lru-cache";
import cachingMethods from "./memoryManagement/cache/writingPolicies/writingPolicy";
import serverException from "./errorHandler";

export class LRU {
    key:any;
    value:any;
    result:any;

    private cache = new LRUCache<string, string>({
      maxSize: 3,
      entryExpirationTimeInMS: 60000,
      onEntryEvicted: ({ key, isExpired }) =>
        console.log(`Entry with key ${key} was evicted from the cache. Expired: ${isExpired}`),
      onEntryMarkedAsMostRecentlyUsed: ({ key}) =>
        console.log(`Entry with key ${key} was just marked as most recently used.`)
    });
  

    async get(key:any) {
        let item = this.cache.get(key);
        if(item) {
            console.log("Get the data from cache...");
            console.log("item",item);
            return item;
        }
        else if(!item) {
            serverException.cahceMissError();
            return  this.getDataFromDb(key);
        }
    }
     
    public async getDataFromDb(key: any) {
      let value: any = await mongoDBManager.getData(key);
        if(this.result === 'writeAround') {
        let response = this.cache.set(key, value);
        if (response) console.log(`Successfully stored data to cache.`);
       }
        return value;
      }


    public async storeDataToDb(key: any, value: any) {
        if(this.result === "writeBack") {
        setTimeout(async () => { await mongoDBManager.insertData(key, value); }, 1000);
      }
      else {
        await mongoDBManager.insertData(key, value);
      }
      }

    lruData() {
        return this.cache.keys().next().value;
    }

    public async set(key: string, value: string) {
      this.result = cachingMethods.checkCachingMethod()
       console.log("result",this.result)
        if( this.result != 'writeAround') {
        let response = this.cache.set(key, value);
        if (response) {
          console.log(`Successfully stored data to cache.`);
        }
        else {
        serverException.cacheWriteError();
        }
      }
      return await this.storeDataToDb(key, value);
      }
}
