class ErrorHandler {

  public cahceMissError() {
      console.log("Data is not available in the cache, retriving it from database...");
    }
  
    public dataMissErrorInDB() {
      console.log("Data is not available in database!");
    }
  
  public  cacheWriteError() {
      console.log("Unable to store data to cache!");
    }
  
    public  dbWriteError() {
      console.log("Unable to store data to database!");
    }
  
  public  connectToDbError() {
      console.log("Unable to established the connection with database!");
    }
  
    public getDataFromDbError() {
      console.log("Unable to get the data from database!");
    }
  
    public runServerError() {
      console.log(`Server Error: Client disconnected!`);
    }

    public keyNotFoundinCacheError() {
      console.log("Data not found in cache, Getting it from database...");
    }

    getClientRequestException(error: Error) {
      console.log(`Server Error: ${error}!`);
    }
}
const serverException : ErrorHandler = new ErrorHandler();
export default serverException;