import writingPolicyHandler from "./writingPolicies/writingPolicy";

export class CacheManager {

    public maxEntries!: number;
    public cachingStratigie!: any;
    public cache: Map<string, string> = new Map<string, string>();
  
    defineLruEntries() {
      this.maxEntries = 10;
    }
  
    defineCachingStrategies() {
      this.cachingStratigie = writingPolicyHandler.cachePollicy(`writeThrough`);
    }
  
    async getData(key: string) {
      return await this.cachingStratigie.getData(key, this.cache);
    }
  
    async storeData(key: string, value: string) {
      return await this.cachingStratigie.storeData(key, value, this.cache, this.maxEntries)
    }
  
    public storeBackupDataToCache(key: string, value: string) {
      this.cache.set(key, value);
    }
  
    public getCacheData() {
      return this.cache;
    } 
  }
  
  const cacheHandler = new CacheManager();
  cacheHandler.defineLruEntries();
  cacheHandler.defineCachingStrategies();
  export default cacheHandler;