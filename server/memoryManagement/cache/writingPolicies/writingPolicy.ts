class CachingTechniques {
  private cacheMethod!: string;
  checkCachingMethod() {
      this.cacheMethod = `writeThrough`;
      this.cachePollicy(this.cacheMethod)
      return this.cacheMethod;
  }

   cachePollicy(cacheMethod:any) {
     if(cacheMethod === 'writeThrough') {
       return 'writeThrough';
     }
     else if(cacheMethod === 'wrteBack') {
       return 'writeBack';
     }
     else if(cacheMethod === 'writeAround')
     return 'writeAround';
   }
  }
  
  const cachingMethods: CachingTechniques = new CachingTechniques();
  cachingMethods.checkCachingMethod();
  export default cachingMethods;
