import serverException from "../../errorHandler";

const MongoClient = require('mongodb').MongoClient;

export class MongoDB {
  dbObject: any;

  connectToDb() {
    try {
      var url = "mongodb://localhost:27017/";
      MongoClient.connect(url, (err: any, db: any) => {
        if (err) throw err;
        console.log("connected to DB")
        this.dbObject = db.db("ICS_DATABASE");
      });
    } catch (error) {
      serverException.connectToDbError();
    }
  }

  async insertData(key: any, value: any) {
    let dataFromClient = { key: key, value: value }
    this.dbObject.collection("ics_data").insertOne(dataFromClient, function (err: any, res: any) {
      if (err) throw err;
      else
        console.log("data stored in DB..")
    })
  }

  getData(key: any) {
    let value;
    try {
      this.dbObject.collection("ics_data").find({}).toArray(function (err: any, result: any) {
        if (err) throw err;
        for (let initialise = 0; initialise < result.length; initialise++) {
          if (result[initialise].key === key) {
            console.log("value", result[initialise].value)
            value = result[initialise].value;
            break;
          }
        }
      });
      return value;
    }
    catch (error) {
     serverException.getDataFromDbError();
    }
  }

}
const mongoDBManager: MongoDB = new MongoDB();
mongoDBManager.connectToDb();
export default mongoDBManager