import * as net from 'net';
import routeManager from "./routes";
import responseHandler from "./response";
import opearionValidation from "./servervalidation";
class RequestHandler {

    key: any;

   async handleClientRequest(socket: net.Socket, clientRequest: string) {
    if (clientRequest) {
        let isGetRequest: boolean = opearionValidation.isGetRequest(clientRequest);
        let isSetRequest: boolean = opearionValidation.isSetRequest(clientRequest);
  
        if (isGetRequest) {
          this.getRequest(clientRequest, socket);
        }
        else if (isSetRequest) {
          this.setRequest(clientRequest, socket);
        }
        else {
          responseHandler.invalidClientRequest(socket);
        }
    }
  }

  async getRequest(clientRequest: string, socket: net.Socket) {
    const key: string = clientRequest.split(" ", 3)[2];
    if (key) {
        routeManager.getData(key, socket);
    }
    else {
      await responseHandler.keyNotFound(socket);
    }
  }

  async setRequest(clientRequest: string, socket: net.Socket) {
    const key: string = clientRequest.split(" ", 3)[2];
    const value: string = clientRequest.split(" ", 4)[3];
    if (key && value) {
        routeManager.setData(key, value, socket);
    }
    else {
      if (!key) {
        await responseHandler.keyNotFound(socket);
      }
      else {
        await responseHandler.valueNotFound(socket);
      }
    }
  }


}

const requestHandler: RequestHandler = new RequestHandler();
export default requestHandler;