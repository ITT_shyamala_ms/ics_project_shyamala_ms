import * as net from 'net';

class ResponseHandler {

    sendResponseToClient(serverResponse: any, socket: any) {
      if (serverResponse) {
       socket.write(serverResponse);
      }
      else {
        console.log('Data is not available!');
      }
    }

    invalidClientRequest(socket: net.Socket) {
        socket.write('Invalid Request');
      }
    
      keyNotFound(socket: net.Socket) {
        socket.write('Key Not Found');
      }
    
      valueNotFound(socket: net.Socket) {
        socket.write('Value Not Found');
      }
    
      sendValueToClient(socket: net.Socket, value: string) {
        if (value) {
          socket.write(value);
        }
        else {
          socket.write(`Data is not available!`);
        }
      }
    
      dataStored(socket: net.Socket) {
        socket.write(`Data stored successfully`);
      }
  
  }
  
  const responseHandler: ResponseHandler = new ResponseHandler();
  export default responseHandler;