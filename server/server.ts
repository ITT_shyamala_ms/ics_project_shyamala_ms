import * as net from 'net';
import RouteHandler from './routes'

export default class ServerConnection {
    port: number;
    host: any
    address: any;
    connection: any;
    server: any
    constructor(port: number, host: string) {
        this.port = port;
        this.host = host;
        this.createServer(this.port, this.host)
        this.createSocket()
    }

    createServer(port: number, host: any) {
        this.server = net.createServer();
        this.server.listen(port, host, () => {
            console.log(`Server is listening on ${host}:${port}`);
        });
    }
    createSocket() {
        let sockets: any[] = [];
        this.server.on('connection', (socket: any) => {
            let clientAddress = `${socket.remoteAddress}:${this.port}`
            console.log(`new client connected: ${clientAddress}`);
            sockets.push(socket);
            socket.on('data', (data: any) => {
                let dataFromClient = JSON.parse(data)
                console.log("Client", dataFromClient);
                if(dataFromClient[0]) {
                }
                else {
                    const routeController = new RouteHandler(dataFromClient.type, dataFromClient.key, dataFromClient.value);
                    routeController.validateClientRequest();
                }
                sockets.forEach((sock) => {
                    sock.write(socket.remoteAddress + ':' + socket.remotePort + " said " + data + '\n');
                });
            });
        });
    }
}
let newServer = new ServerConnection(4500, "localhost")