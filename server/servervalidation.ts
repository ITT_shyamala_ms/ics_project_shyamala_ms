class ServerValidation {

    isGetRequest(clientRequest: string) {
      return (clientRequest.split(" ", 1)[0] == 'ics' && clientRequest.split(" ", 2)[1] == 'GET');
    }
  
    isSetRequest(clientRequest: string) {
      return (clientRequest.split(" ", 1)[0] == 'ics' && clientRequest.split(" ", 2)[1] == 'SET');
    }
  
  }
  
  const operationValidation: ServerValidation = new ServerValidation();
  export default operationValidation;